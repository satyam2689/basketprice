package com.price.basket;

import java.util.ArrayList;

import com.price.basket.bean.BeanCartItemDetails;

public class CartDataHelper {

	private static CartDataHelper instance;
	private static ArrayList<BeanCartItemDetails> arrayListItemDetails;

	private CartDataHelper() { }

	public synchronized static CartDataHelper getInstance() {
		if (instance == null) {
			instance = new CartDataHelper();
			arrayListItemDetails = new ArrayList<BeanCartItemDetails>();
		}
		return instance;
	}
	
	public BeanCartItemDetails getListItem(int index) {
		return arrayListItemDetails.get(index);
	}

	public void addListItem(BeanCartItemDetails beanItemDetails) {
		arrayListItemDetails.add(beanItemDetails);
	}

	public ArrayList<BeanCartItemDetails> getListItems() {
		return arrayListItemDetails;
	}

	public void remomeListItem(int index) {
		arrayListItemDetails.remove(index);
	}

	public void remomeAllListItem() {
		arrayListItemDetails = new ArrayList<BeanCartItemDetails>();
	}

	public int getListItemCount() {
		return arrayListItemDetails.size();
	}

	public void clearInstance() {
		instance = null;
	}
	
	public boolean isProductAlreadyAdded(int id)
	{
		for(int i = 0 ; i < arrayListItemDetails.size() ; i++ )
		{
			if(arrayListItemDetails.get(i).getItemId() == id)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public String getAllItemPrice() {
		double p = 0;

		try {
			for (int i = 0; i < getListItemCount(); i++) {
				BeanCartItemDetails item = arrayListItemDetails.get(i);
				double _price = item.getPrice();
				int _qty = item.getQty();
				p = p + (_price * Integer.valueOf(_qty));
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		String formattedPrice = String.format("%.02f", p);

		return formattedPrice;
	}
}
