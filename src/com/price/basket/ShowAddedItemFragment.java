package com.price.basket;

import java.util.ArrayList;

import com.price.basket.adapter.ShowAddedItemListAdapter;
import com.price.basket.bean.BeanCartItemDetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ShowAddedItemFragment extends Fragment implements IItemAddMinus {

	private ListView listItems;
	private ShowAddedItemListAdapter adapter;
	private ArrayList<BeanCartItemDetails> arrayListItems;
	private Button ivAddItem;
	private LinearLayout btnCart;
	private TextView tvTotal;
	private IItemAddMinus iItemAddMinus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		arrayListItems = new ArrayList<BeanCartItemDetails>();
		arrayListItems = CartDataHelper.getInstance().getListItems();
		iItemAddMinus = this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.showaddeditemfragment, container, false);

		listItems = (ListView) rootView.findViewById(R.id.showaddeditem_listView);
		ivAddItem = (Button) rootView.findViewById(R.id.showaddeditem_iv_add);
		btnCart = (LinearLayout) rootView.findViewById(R.id.showaddeditem_ll_cart);
		tvTotal = (TextView) rootView.findViewById(R.id.showaddeditem_tv_total);

		adapter = new ShowAddedItemListAdapter(getActivity(), arrayListItems, iItemAddMinus);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		listItems.setAdapter(adapter);
		ivAddItem.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				ItemFragment fragment = new ItemFragment();
				Bundle bundleArg = new Bundle();
				fragment.setArguments(bundleArg);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

			}
		});

		updateCartTotal();

		btnCart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().getSupportFragmentManager().beginTransaction()
						.replace(R.id.container, new CheckoutFragment()).addToBackStack(null).commit();
			}
		});

	}

	private void updateCartTotal() {
		if (arrayListItems.isEmpty()) {

			ItemFragment fragment = new ItemFragment();
			Bundle bundleArg = new Bundle();
			fragment.setArguments(bundleArg);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

		} else {

			btnCart.setVisibility(View.VISIBLE);

			String price = CartDataHelper.getInstance().getAllItemPrice();
			tvTotal.setText(getActivity().getString(R.string.pound) + price);
		}
	}

	@Override
	public void itemAdd(int position) {

		View v = listItems.getAdapter().getView(position, null, listItems);
		TextView tvQty = (TextView) v.findViewById(R.id.li_showaddeditem_qty);
		int qty = Integer.valueOf(tvQty.getText().toString());

		if (qty < 50) {
			int updatedQty = qty + 1;
			tvQty.setText(String.valueOf(updatedQty));
			CartDataHelper.getInstance().getListItem(position).setUpdatedQty(updatedQty);
			adapter.notifyDataSetChanged();
			updateCartTotal();

		}
	}

	@Override
	public void itemMinus(int position) {

		View v = listItems.getAdapter().getView(position, null, listItems);
		TextView tvQty = (TextView) v.findViewById(R.id.li_showaddeditem_qty);
		int qty = Integer.valueOf(tvQty.getText().toString());

		int updatedQty = qty - 1;
		tvQty.setText(String.valueOf(updatedQty));
		arrayListItems.get(position).setUpdatedQty(updatedQty);

		if (updatedQty == 0) {
			// arrayListItems.remove(position);
			CartDataHelper.getInstance().remomeListItem(position);

		} else {
			CartDataHelper.getInstance().getListItem(position).setUpdatedQty(updatedQty);
		}
		adapter.notifyDataSetChanged();

		updateCartTotal();
	}
}
