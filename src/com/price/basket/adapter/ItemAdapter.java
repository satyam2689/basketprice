package com.price.basket.adapter;

import java.util.ArrayList;

import com.price.basket.R;
import com.price.basket.R.id;
import com.price.basket.R.layout;
import com.price.basket.bean.BeanItemData;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter {
	ArrayList<BeanItemData> arrayList;
	Context context;
	ViewHolder holder = null;

	public ItemAdapter(Context context, ArrayList<BeanItemData> arrayList) {
		this.context = context;
		this.arrayList = arrayList;
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.listitem_itemadapter, null);
			holder.tvNames = (TextView) convertView.findViewById(R.id.li_itemadapter_name);
			holder.tvUnit = (TextView) convertView.findViewById(R.id.li_itemadapter_unit);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvNames.setText(arrayList.get(position).getName().trim());
		holder.tvUnit.setText(arrayList.get(position).getUnit());

		return convertView;
	}

	class ViewHolder {
		TextView tvNames;
		TextView tvUnit;
	}
}