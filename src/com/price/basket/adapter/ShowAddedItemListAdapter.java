package com.price.basket.adapter;

import java.util.ArrayList;

import com.price.basket.IItemAddMinus;
import com.price.basket.R;
import com.price.basket.R.id;
import com.price.basket.R.layout;
import com.price.basket.bean.BeanCartItemDetails;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowAddedItemListAdapter extends BaseAdapter {
	ArrayList<BeanCartItemDetails> arrayList;
	Context context;
	private IItemAddMinus iItemAddMinus;
	ViewHolder holder = null;

	public ShowAddedItemListAdapter(Context context, ArrayList<BeanCartItemDetails> arrayList,
			IItemAddMinus iCatalogItemAddMinus) {
		this.context = context;
		this.arrayList = arrayList;
		this.iItemAddMinus = iCatalogItemAddMinus;
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.listitem_showaddeditem, null);
			holder.tvDetails = (TextView) convertView.findViewById(R.id.li_showaddeditem_detail);
			holder.tvQty = (TextView) convertView.findViewById(R.id.li_showaddeditem_qty);
			holder.tvPrice = (TextView) convertView.findViewById(R.id.li_showaddeditem_price);
			holder.ivMinus = (ImageView) convertView.findViewById(R.id.li_showaddeditem_qty_ivminus);
			holder.ivAdd = (ImageView) convertView.findViewById(R.id.li_showaddeditem_qty_ivadd);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvDetails.setText(arrayList.get(position).getName());
		holder.tvQty.setText(String.valueOf(arrayList.get(position).getQty()));

		double price = arrayList.get(position).getPrice();
		holder.tvPrice.setText(String.valueOf(price));

		holder.ivAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				iItemAddMinus.itemAdd(position);
			}
		});

		holder.ivMinus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				iItemAddMinus.itemMinus(position);
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView tvDetails;
		TextView tvQty;
		TextView tvPrice;
		ImageView ivMinus;
		ImageView ivAdd;
	}

}