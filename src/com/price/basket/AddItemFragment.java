package com.price.basket;

import com.price.basket.bean.BeanCartItemDetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AddItemFragment extends Fragment implements OnClickListener {
	private TextView tvItemName;
	private ImageView ivMinus;
	private TextView tvQty;
	private ImageView ivAdd;
	private Button btnAdd;

	private String itemName;
	private double itemPrice;
	private String itemUnit;
	private TextView tvPrice;
	private int itemId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundleArg = getArguments();

		if (bundleArg != null && (!bundleArg.isEmpty())) {
			itemId = bundleArg.getInt("itemId");
			itemName = bundleArg.getString("itemName");
			itemPrice = bundleArg.getDouble("itemPrice");
			itemUnit = bundleArg.getString("itemUnit");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.addnewitemfragment, container, false);

		tvItemName = (TextView) rootView.findViewById(R.id.addnewitem_detail);
		ivMinus = (ImageView) rootView.findViewById(R.id.addnewitem_ivminus);
		tvQty = (TextView) rootView.findViewById(R.id.addnewitem_qty);
		ivAdd = (ImageView) rootView.findViewById(R.id.addnewitem_ivadd);
		btnAdd = (Button) rootView.findViewById(R.id.addnewitem_btnadd);
		tvPrice = (TextView) rootView.findViewById(R.id.addnewitem_price);

		ivMinus.setOnClickListener(this);
		ivAdd.setOnClickListener(this);
		btnAdd.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		tvPrice.setText(String.valueOf(itemPrice));
		tvItemName.setText(itemName);

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.addnewitem_ivminus: {
			int qty = Integer.valueOf(tvQty.getText().toString());
			if (!(qty == 1)) {
				int updatedQty = qty - 1;
				tvQty.setText(String.valueOf(updatedQty));
			}
		}
			break;
		case R.id.addnewitem_ivadd: {
			int qty = Integer.valueOf(tvQty.getText().toString());
			if (qty < 50) {
				int updatedQty = qty + 1;
				tvQty.setText(String.valueOf(updatedQty));
			}
		}
			break;
		case R.id.addnewitem_btnadd: {
			setCartData();
		}
			break;

		default:
			break;
		}
	}

	private void setCartData() {

		CartDataHelper cart = CartDataHelper.getInstance();
		String txtQty = tvQty.getText().toString();

		BeanCartItemDetails beanItemDetails = new BeanCartItemDetails();
		beanItemDetails.setData(itemId, itemName, Integer.parseInt(txtQty), itemUnit, itemPrice);
		cart.addListItem(beanItemDetails);

		Fragment fragment = new ShowAddedItemFragment();
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
	}
}
