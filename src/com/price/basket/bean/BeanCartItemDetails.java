package com.price.basket.bean;

public class BeanCartItemDetails {

	private int itemId;
	private String name;
	private int qty;
	private String unit;
	private double price;

	public BeanCartItemDetails() {
		
	}

	public void setData(int itemId, String name, int qty, String unit, double price) {

		this.itemId = itemId;
		this.name = name;
		this.qty = qty;
		this.unit = unit;
		this.price = price;
	}

	public int getItemId() {
		return itemId;
	}

	public String getName() {
		return name;
	}

	public String getUnit() {
		return unit;
	}

	public int getQty() {
		return qty;
	}

	public double getPrice() {
		return price;
	}

	public void setUpdatedQty(int _qty) {
		this.qty = _qty;
	}

}
