package com.price.basket.bean;

public class BeanItemData {

	private int itemId;
	private String name;
	private String unit;
	private double price;

	public BeanItemData(int itemId, String name, String unit, double price) {
		this.itemId = itemId;
		this.name = name;
		this.unit = unit;
		this.price = price;
	}

	public int getItemId() {
		return itemId;
	}

	public String getName() {
		return name;
	}

	public String getUnit() {
		return unit;
	}

	public double getPrice() {
		return price;
	}

}
