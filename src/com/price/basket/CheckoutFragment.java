package com.price.basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class CheckoutFragment extends Fragment implements OnItemSelectedListener {

	private ProgressDialog pdialog;
	private TextView tvTotalPrice;
	private TextView tvConvertedPrice;
	private Spinner spinnerCurrency;
	private List<String> currenyList;
	private ArrayAdapter<String> currencyAdapter;
	private HashMap<String, Double> currencyHashMap;
	private String tPrice;

	public CheckoutFragment() {
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.checkout_fragment, container, false);

		tvTotalPrice = (TextView)rootView.findViewById(R.id.checkout_totalprice);
		tvConvertedPrice = (TextView)rootView.findViewById(R.id.checkout_convertedprice);
		spinnerCurrency = (Spinner)rootView.findViewById(R.id.checkout_currencySpinner);
		
		spinnerCurrency.setOnItemSelectedListener(this);
		
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		currencyHashMap = new HashMap<String, Double>();
		
		tPrice = CartDataHelper.getInstance().getAllItemPrice();
		tvTotalPrice.setText(getActivity().getString(R.string.pound) + tPrice);
		
		getCurrencyLists();
	}

	public void getCurrencyLists() {
		
		currenyList = new ArrayList<String>();

		pdialog = new ProgressDialog(getActivity());
		pdialog.setMessage("Please wait...");
		pdialog.show();

		String url = "http://api.fixer.io/latest?base=GBP";//
		StringRequest request = new StringRequest(Method.GET, url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {

				pdialog.dismiss();
				
				Log.d("respoce", arg0);
				
				try {
					JSONObject jsonObject = new JSONObject(arg0);
					
					Log.d("JSON", jsonObject.toString());
					JSONObject jsonCurrency = jsonObject.getJSONObject("rates");
					Log.d("rates", jsonCurrency.toString());
					
					Iterator<String> iter = jsonCurrency.keys();
			        while (iter.hasNext())
			        {
			            String key = iter.next();
			            Log.d("--------iterator key-------: " , key);
			            try
			            {
			                double value = jsonCurrency.getDouble(key);
			                currenyList.add(key);
			                currencyHashMap.put(key, value);			                
			            } catch (Exception e) {
			                e.printStackTrace();
			            }
			        }
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				currencyAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, currenyList);
				spinnerCurrency.setAdapter(currencyAdapter);

			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				pdialog.dismiss();
			
			}
		}) {
			

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				//
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-type", "application/x-www-form-urlencoded");
				return headers;
			}
		};
		// api takes time in given responce, thats why use retry policy.
		request.setRetryPolicy(new DefaultRetryPolicy(volleyTimeout, volleyRetry, volleyBackOff));
		AppController.getInstance().addToRequestQueue(request);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		
		String key = currenyList.get(position);
		double c = currencyHashMap.get(key);
		
		Log.d(key, String.valueOf(c));
		
		double currencyChangedPrice = Double.parseDouble(tPrice) * c ;
		String formattedPrice = String.format("%.02f", currencyChangedPrice);
		tvConvertedPrice.setText(String.valueOf(formattedPrice));
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		
	}
	
	public static int volleyTimeout = 10000;
	public static int volleyRetry = 3;
	public static float volleyBackOff = DefaultRetryPolicy.DEFAULT_BACKOFF_MULT;
}