package com.price.basket;

import java.util.ArrayList;

import com.price.basket.adapter.ItemAdapter;
import com.price.basket.bean.BeanItemData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ItemFragment extends Fragment{
	private ListView listView;
	private ItemAdapter adapterItem;
	private ArrayList<BeanItemData> arrayListMenuItem;
	private BeanItemData beanCatalogMenuItem = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		arrayListMenuItem = new ArrayList<BeanItemData>();
		
		
		beanCatalogMenuItem = new BeanItemData(1,"Peas","95p per bag",95);
		arrayListMenuItem.add(beanCatalogMenuItem);
		
		beanCatalogMenuItem = new BeanItemData(2,"Eggs",getActivity().getString(R.string.pound) + "2.10 per dozen",2.10);
		arrayListMenuItem.add(beanCatalogMenuItem);
		
		beanCatalogMenuItem = new BeanItemData(3,"Milk",getActivity().getString(R.string.pound) + "1.30 per bottel",1.30);
		arrayListMenuItem.add(beanCatalogMenuItem);
		
		beanCatalogMenuItem = new BeanItemData(4,"Beans","73p per can",73);
		arrayListMenuItem.add(beanCatalogMenuItem);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.itemfragment, container, false);
		listView = (ListView) v.findViewById(R.id.item_listview);


		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		adapterItem = new ItemAdapter(getActivity(), arrayListMenuItem);
		listView.setAdapter(adapterItem);

		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int itemId = arrayListMenuItem.get(position).getItemId();
				String txtDetials = arrayListMenuItem.get(position).getName();
				double txtPrice = arrayListMenuItem.get(position).getPrice();
				String txtUnit = arrayListMenuItem.get(position).getUnit();
				
				if(CartDataHelper.getInstance().isProductAlreadyAdded(itemId))
				{
					Fragment fragment = new ShowAddedItemFragment();
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
				}
				else
				{
					AddItemFragment fragment = new AddItemFragment();
					Bundle bundleArg = new Bundle();
					bundleArg.putInt("itemId", itemId);
					bundleArg.putString("itemName", txtDetials);
					bundleArg.putDouble("itemPrice", txtPrice);
					bundleArg.putString("itemUnit", txtUnit);

					fragment.setArguments(bundleArg);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
				}
			}
		});
	}



}
