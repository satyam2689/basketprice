package com.price.basket;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class BasketActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new ItemFragment()).commit();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		CartDataHelper.getInstance().clearInstance();

	}

}
