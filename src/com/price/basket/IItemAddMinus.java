package com.price.basket;

public interface IItemAddMinus {
	
	public void itemAdd(int position);	
	public void itemMinus(int position);	

}
